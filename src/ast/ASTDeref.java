package ast;

import compiler.CodeBlock;
import types.IType;
import types.RefType;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.RefValue;

public class ASTDeref implements ASTNode {
	
	ASTNode expr;
	IType valueType;
	
	
	public ASTDeref(ASTNode expr){
		this.expr = expr;
	}
	
	@Override
	public IValue eval(Environment<IValue> env)	throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		RefValue ref = (RefValue) expr.eval(env);
		return ref.getValue();
	}

	@Override
	public void compile(CodeBlock code) {
		// TODO Auto-generated method stub
		
	}
	
	public String toString() {
    	return "*" + expr.toString() ;
    }

	@Override
	public IType typecheck(Environment<IType> environment) throws UndeclaredIdentifierException, DuplicateIdentifierException, TypeErrorException {
		IType t1 = expr.typecheck(environment);
		
		if( t1 instanceof RefType ) {
			this.valueType = ((RefType) t1).getType();
			return this.valueType;
		}
		else 
			throw new TypeErrorException("Trying to deref a non-reference value");
	}

}

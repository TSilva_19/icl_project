package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;

public class ASTAnd implements ASTNode {

	ASTNode left, right;

	public ASTAnd(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		BoolValue v1 = (BoolValue) left.eval(env);
    	BoolValue v2 = (BoolValue) right.eval(env);
		if(v1.getValue() && v2.getValue()){
			return new BoolValue(true);
		}
		else {return new BoolValue(false);}
	}
	
	public String toString() {
		return left.toString() + " && " + right.toString();
	}

	
	@Override
	public void compile(CodeBlock code) {
		left.compile(code);
		right.compile(code);
		//code.emit_and();
	}

	@Override
	public IType typecheck(Environment<IType> environment) throws UndeclaredIdentifierException, DuplicateIdentifierException, TypeErrorException {
		IType t1 = left.typecheck(environment);
		IType t2 = right.typecheck(environment);
		
		if(t1 == BoolType.value && t2 == BoolType.value)
			return BoolType.value;
		else 
			throw new TypeErrorException("Type mismatch on '&&' operator.");
		
	}

	
}
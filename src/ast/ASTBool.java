package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;

public class ASTBool implements ASTNode {

	Boolean bool;

	public ASTBool(Boolean bool) {
		this.bool = bool;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		return new BoolValue(bool); 
	}

	@Override
	public String toString() {
		return Boolean.toString(bool);
	}

	@Override
	public void compile(CodeBlock code) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public IType typecheck(Environment<IType> environment) throws UndeclaredIdentifierException, DuplicateIdentifierException, TypeErrorException {
	return BoolType.value;
	}
}

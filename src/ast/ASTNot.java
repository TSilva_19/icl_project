package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;

public class ASTNot implements ASTNode {

	ASTNode fact;

	public ASTNot(ASTNode fact) {
		this.fact = fact;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		BoolValue v1 = (BoolValue) fact.eval(env);
		if(v1.getValue() == (true)){
		 return new BoolValue (false);}
		else{
			return new BoolValue(true);}
	}

	@Override
	public String toString() {
		return "!" + fact.toString();
	}

	@Override
	public void compile(CodeBlock code) {
		
		
	}

	@Override
	public IType typecheck(Environment<IType> environment) throws UndeclaredIdentifierException, DuplicateIdentifierException, TypeErrorException {
		IType t1 = fact.typecheck(environment);
				
		if(t1 == BoolType.value)
			return BoolType.value;
		else 
			throw new TypeErrorException("Type mismatch on '!' operator.");
		
	}
	
	

	
}
package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.IntType;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;
import values.IntValue;

public class ASTBigger_Equal implements ASTNode {

	ASTNode left, right;

	public ASTBigger_Equal(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}
	
	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		
		IntValue v1 = (IntValue) left.eval(env);
    	IntValue v2 = (IntValue) right.eval(env);
		if(v1.getValue() >= v2.getValue()){
			return new BoolValue(true);
		}
		else {return new BoolValue(false);}
	}

	@Override
	public void compile(CodeBlock code) {
		left.compile(code);
		right.compile(code);
		//code.emit_bigger_equal();
	}

	@Override
	public String toString() {
		return left.toString() + " >= " + right.toString();
	}

	@Override
	public IType typecheck(Environment<IType> environment) throws UndeclaredIdentifierException, DuplicateIdentifierException, TypeErrorException {
		IType t1 = left.typecheck(environment);
		IType t2 = right.typecheck(environment);
		
		if(t1 == IntType.value && t2 == IntType.value)
			return BoolType.value;
		else
			throw new TypeErrorException("Type mismatch on '>=' operator.");
	}
	
}

package ast;

import compiler.CodeBlock;
import types.IType;
import types.IntType;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;

public class ASTDiv implements ASTNode {

	ASTNode left, right;

	public ASTDiv(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		return new IntValue(((IntValue)left.eval(env)).getValue() /
				((IntValue)right.eval(env)).getValue());
	}

	@Override
	public void compile(CodeBlock code) {
		this.left.compile(code);
		this.right.compile(code);
		code.emit_div();
	}

	@Override
	public String toString() {
		return left.toString() + " / " + right.toString();
	}

	@Override
	public IType typecheck(Environment<IType> environment) throws UndeclaredIdentifierException, DuplicateIdentifierException, TypeErrorException {
		IType t1 = left.typecheck(environment);
		IType t2 = right.typecheck(environment);
		
		if(t1 == IntType.value && t2 == IntType.value)
			return IntType.value;
		else
			throw new TypeErrorException("Type mismatch on '/' operator.");
	}
}

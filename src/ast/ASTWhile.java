package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;

public class ASTWhile implements ASTNode{
	
	ASTNode condition, body;
	
	public ASTWhile(ASTNode condition, ASTNode body){
		this.condition = condition;
		this.body = body;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		BoolValue v1 = (BoolValue) condition.eval(env);
		IValue val = null;
		
		while(v1.getValue() ) {
	    	   val = condition.eval(env);
	    }
		return val;
	}

	@Override
	public void compile(CodeBlock code) {
		// TODO Auto-generated method stub
		
	}
	
	 public String toString() {
	    	return "while " + condition.toString() + " do " + body.toString() + " end" ;
	    }

	@Override
	public IType typecheck(Environment<IType> environment) throws UndeclaredIdentifierException, DuplicateIdentifierException, TypeErrorException {
		IType t1 = condition.typecheck(environment);
		IType t2 = null;
		
		if (t1 == BoolType.value){
			t2 = body.typecheck(environment);
			return t2;
		}
		else
			throw new TypeErrorException("Type mismatch on 'while' operator.");
	}

}

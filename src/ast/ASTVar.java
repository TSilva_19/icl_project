package ast;

import compiler.CodeBlock;
import types.IType;
import types.RefType;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.RefValue;

public class ASTVar implements ASTNode{
	
	ASTNode val;
	
	public ASTVar(ASTNode val){
		this.val = val;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		return new RefValue(val.eval(env));
	}

	@Override
	public void compile(CodeBlock code) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
    public String toString() {
    	return "var(" + val.toString() + ")";
    }

	@Override
	public IType typecheck(Environment<IType> environment) throws UndeclaredIdentifierException, DuplicateIdentifierException, TypeErrorException {
		IType t1 = val.typecheck(environment);
		
		IType valueType = t1; // store the type to be used by the compile method
		
		return new RefType(t1);// TODO Auto-generated method stub
		
	}

}

package ast;

import compiler.CodeBlock;
import types.IType;
import types.IntType;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;

public class ASTNegative implements ASTNode {

	ASTNode number;

	public ASTNegative(ASTNode number) {
		this.number = number;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		return new IntValue(- ((IntValue) number.eval(env)).getValue());
	}
	
	@Override
	public void compile(CodeBlock code) {
		number.compile(code);
		//code.emit_negative();
	}

	@Override
	public String toString() {
		return "-" + number.toString();
	}

	@Override
	public IType typecheck(Environment<IType> environment) throws UndeclaredIdentifierException, DuplicateIdentifierException, TypeErrorException {
		IType t1 = number.typecheck(environment);
		
		if(t1 == IntType.value)
			return IntType.value;
		else
			throw new TypeErrorException("Type mismatch on '-' operator.");
	}
}

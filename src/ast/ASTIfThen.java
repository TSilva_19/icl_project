package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.IntType;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;

public class ASTIfThen implements ASTNode{
	
	ASTNode condition, body, alternative;
	
	public ASTIfThen(ASTNode condition, ASTNode body, ASTNode alternative){
		this.condition = condition;
		this.body = body;
		this.alternative = alternative;
	}

	@Override
	public IValue eval(Environment<IValue> env)	throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		BoolValue v1 = (BoolValue) condition.eval(env);
		IValue v2 = (IValue) body.eval(env);
		IValue v3 = (IValue) alternative.eval(env);
		
		if(v1.getValue()){
			return v2;
		}
		else{
			return v3;
		}
	}

	@Override
	public void compile(CodeBlock code) {
		// TODO Auto-generated method stub
		
	}
	
	public String toString() {
	    return "if " + condition.toString() + " then " + body.toString() + " else " + alternative.toString() + " end" ;
	}

	@Override
	public IType typecheck(Environment<IType> environment) throws UndeclaredIdentifierException, DuplicateIdentifierException, TypeErrorException {
		IType t1 = condition.typecheck(environment);
		IType t2 = body.typecheck(environment);
		IType t3 = alternative.typecheck(environment);
		
		if (t1 == BoolType.value)
		{
			if (t2 == IntType.value || t3 == IntType.value){
				return IntType.value;
			}
			else{
				return BoolType.value;
			}
		}
		else
			throw new TypeErrorException("Type mismatch on 'if' operator.");
	}

}

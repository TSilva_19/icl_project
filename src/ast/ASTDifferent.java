package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.IntType;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;
import values.IntValue;

public class ASTDifferent implements ASTNode {

	ASTNode left, right;

	public ASTDifferent(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		
		if(left.eval(env) instanceof IntValue && right.eval(env) instanceof IntValue){
			return new BoolValue(((IntValue) left.eval(env)).getValue() != ((IntValue) right.eval(env)).getValue());
		}
		else {
			return new BoolValue(((BoolValue) left.eval(env)).getValue() != ((BoolValue) right.eval(env)).getValue());
		}
	}

	@Override
	public String toString() {
		return left.toString() + " != " + right.toString();
	}

	@Override
	public void compile(CodeBlock code) {
		left.compile(code);
		right.compile(code);
		//code.emit_different();
		
	}

	@Override
	public IType typecheck(Environment<IType> environment) throws UndeclaredIdentifierException, DuplicateIdentifierException, TypeErrorException {
		IType t1 = left.typecheck(environment);
		IType t2 = right.typecheck(environment);
		
		if((t1 == IntType.value && t2 == IntType.value) || (t1 == BoolType.value && t2 == BoolType.value))
			return BoolType.value;
		else
			throw new TypeErrorException("Type mismatch on '!=' operator.");
	}

	
}

package ast;

import ast.value.Closure;
import compiler.CodeBlock;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.*;

public class ASTCall implements ASTNode {

	ASTNode left, right;

	public ASTCall(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		Closure c = (Closure) left.eval(env);
		IValue arg = right.eval(env);
		
		Environment<IValue> newenv = c.getEnv();
		newenv = newenv.beginScope();
		newenv.assoc(c.getParameter(), arg);
		return c.getBody().eval(newenv);
	}
	
	@Override
	public void compile(CodeBlock code) {
	}

	@Override
	public String toString() {
		return left.toString() + " ( " + right.toString() + ")";
	}
}

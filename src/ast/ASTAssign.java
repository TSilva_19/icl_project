package ast;

import compiler.CodeBlock;
import types.IType;
import types.RefType;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.RefValue;

public class ASTAssign implements ASTNode {
	
	ASTNode left, right;
	
	public ASTAssign(ASTNode l, ASTNode r){
		left = l;
		right = r;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		RefValue ref = (RefValue) left.eval(env);
		IValue val = right.eval(env);
		ref.setValue(val);
		return val; 
	}

	@Override
	public void compile(CodeBlock code) {
		// TODO Auto-generated method stub
		
	}
	
	public String toString() {
	   return left.toString() + " := " + right.toString();
	}

	@Override
	public IType typecheck(Environment<IType> environment) throws UndeclaredIdentifierException, DuplicateIdentifierException, TypeErrorException {
		
		RefType t1 = (RefType) left.typecheck(environment);
		IType t2 = right.typecheck(environment);
		
		if( t1.getType().equals(t2)) {
			return t2;
		}
		else
			throw new TypeErrorException("Type mismatch on assign.");
		
		
	}

}

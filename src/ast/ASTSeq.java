package ast;

import compiler.CodeBlock;
import types.IType;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.TypeErrorException;
import util.UndeclaredIdentifierException;
import values.IValue;

public class ASTSeq implements ASTNode{
	
	ASTNode left,rigth;
		
	public ASTSeq(ASTNode l, ASTNode r){
		this.left = l;
		this.rigth = r;
	}

	@Override
	public IValue eval(Environment<IValue> env)	throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		left.eval(env);
		return rigth.eval(env);
	}

	@Override
	public void compile(CodeBlock code) {
		// TODO Auto-generated method stub
		
	}
	
	public String toString() {
    	return left.toString() + " ; " + rigth.toString() ;
    }

	@Override
	public IType typecheck(Environment<IType> environment) throws UndeclaredIdentifierException, DuplicateIdentifierException, TypeErrorException {
		
		IType t2 = rigth.typecheck(environment);
		
		return t2;
	}

}

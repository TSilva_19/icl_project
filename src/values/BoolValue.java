package values;

public class BoolValue implements IValue {

	private boolean val;

	public BoolValue(boolean val){
		this.val = val;
	}
	
	public boolean getValue(){
		return val;
	}
	
	public String toString(){
		return Boolean.toString(val);
	}
	
	
}

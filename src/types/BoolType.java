package types;

public class BoolType implements IType {
	
	public static final BoolType value = new BoolType();
	
	private BoolType() {}
	
	public String toString() { return "Boolean"; }

}

package types;

public class RefType implements IType {

	final IType type;
	
	public RefType(IType type) {
		this.type = type;
	}
	
	public String toString() { return "Ref("+type+")"; }

	public IType getType() {
		return type;
	}
	
	public boolean equals(Object other) {
		if( other instanceof RefType ) {
			IType typeOfOther = ((RefType) other).getType();
			return this.type.equals(typeOfOther);
		} else 
			return false;
	}

	
	
}

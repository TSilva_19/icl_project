package types;

public class IntType implements IType {
	
	public static final IntType value = new IntType();
	
	private IntType() {}
	
	public String toString() { return "Integer"; }

}

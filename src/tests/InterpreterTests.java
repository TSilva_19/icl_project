package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import main.Console;
import parser.ParseException;
import values.BoolValue;
import values.IValue;
import values.IntValue;

public class InterpreterTests {

	private void testCase(String expression, IValue value) throws ParseException {
		assertTrue(Console.acceptCompare(expression,value));		
	}
	
	private void testNegativeCase(String expression, IValue value) throws ParseException {
		assertFalse(Console.acceptCompare(expression,value));
	}
	
	//DIGITS TESTS - LAB02
	@Test
	public void test_Digits_01() throws Exception {
		testCase("0\n",new IntValue(0));
		testCase("-0\n",new IntValue(0));
		testCase("1\n",new IntValue(1));
		testCase("1999\n",new IntValue(1999));
		testCase("-1\n",new IntValue(-1));
		testCase("-9891\n",new IntValue(-9891));
		testCase("-(10)\n",new IntValue(-10));
		testCase("-(-10)\n",new IntValue(10));
	}
	
	//ARITHMETIC TESTS - LAB02
	@Test //PLUS
	public void test_Arithmetic_01() throws Exception {
		testCase("1+2\n", new IntValue(3));
		testCase("10+5+6+8+2\n", new IntValue(31));
		testCase("10+(5+2)+3\n", new IntValue(20));
		testCase("-6+5\n", new IntValue(-1));
		testCase("105+(-56)\n", new IntValue(49));
		testCase("-1500+(-16525)\n", new IntValue(-18025));
	}
	
	@Test //MINUS
	public void test_Arithmetic_02() throws Exception {
		testCase("5-10\n", new IntValue(-5));
		testCase("56-41\n", new IntValue(15));
		testCase("10-5-6-8-2\n", new IntValue(-11));
		testCase("10-(5-2)-3\n", new IntValue(4));
		testCase("-(6)-5\n", new IntValue(-11));
		testCase("-1500-(-16525)\n", new IntValue(15025));
	}
	
	@Test //TIMES
	public void test_Arithmetic_03() throws Exception {
		testCase("0*15\n", new IntValue(0));
		testCase("10*15\n", new IntValue(150));
		testCase("56*(-41)\n", new IntValue(-2296));
		testCase("10*5*6*8*2\n", new IntValue(4800));
		testCase("60*(5*2)*3\n", new IntValue(1800));
		testCase("-(6)*5\n", new IntValue(-30));
		testCase("-1500*(-6)\n", new IntValue(9000));
		testCase("60*((5*2)*3)*(5)\n", new IntValue(9000));
	}
	
	@Test //DIV
	public void test_Arithmetic_04() throws Exception {
		testCase("0/15\n", new IntValue(0));
		testCase("15/10\n", new IntValue(1));
		testCase("56/(-41)\n", new IntValue(-1));
		testCase("1000/5/6/8/2\n", new IntValue(2));
		testCase("-(25)/5\n", new IntValue(-5));
		testCase("-1500/(-6)\n", new IntValue(250));
	}
	
	@Test //ALL
	public void test_Arithmetic_05() throws Exception {
		testCase("4+2-5+8\n", new IntValue(9));
		testCase("5*3-6+9-7*2\n", new IntValue(4));
		testCase("5*(3-6+9)-7*2\n", new IntValue(16));
		testCase("1*4+5/5-5\n", new IntValue(0));
		testCase("4*2+1-5/2*1+2\n", new IntValue(9));
		testCase("-(4*5)+(((8-2)/(2*1))*3)\n", new IntValue(-11));
	}
	
	//TRUE/FALSE TESTS - LAB02
	@Test
	public void test_True_False_01() throws Exception {
		testCase("true\n", new BoolValue(true));
		testCase("false\n", new BoolValue(false));
		testCase("!false\n", new BoolValue(true));
		testCase("!true\n", new BoolValue(false));
		testCase("!!false\n", new BoolValue(false));
		testCase("!!true\n", new BoolValue(true));
	}
	
	//BOOLEAN TESTS - LAB02
	@Test //AND
	public void test_Boolean_01() throws Exception {
		testCase("true&&true\n", new BoolValue(true));
		testCase("true&&false\n", new BoolValue(false));
		testCase("false&&true\n", new BoolValue(false));
		testCase("false&&false\n", new BoolValue(false));
		testCase("false&&true&&false\n", new BoolValue(false));
		testCase("false&&(true&&true)\n", new BoolValue(false));
	}
	
	@Test //OR
	public void test_Boolean_02() throws Exception {
		testCase("true||true\n", new BoolValue(true));
		testCase("true||false\n", new BoolValue(true));
		testCase("false||true\n", new BoolValue(true));
		testCase("false||false\n", new BoolValue(false));
		testCase("false||true||false\n", new BoolValue(true));
		testCase("false||(true||true)\n", new BoolValue(true));
	}
	
	@Test //ALL
	public void test_Boolean_03() throws Exception {
		testCase("(true||true)&&(false||false)\n", new BoolValue(false));
		testCase("(true&&false)||(false&&(!false))\n", new BoolValue(false));
		testCase("false&&true||false\n", new BoolValue(false));
		testCase("false||(!false&&!false)&&true\n", new BoolValue(true));
	}
	
	//COMPARATION TESTS - LAB02 
	
	
	//IF THEN TESTS - LAB06
	
	//WHILE TESTS - LAB06
	
	//ASSIGN TESTS - LAB06
	
	
	
	
	
//	@Test
//	public void testsLabClass04() throws Exception {
//		testCase("decl x = decl y = 3 in 10*y end in x + decl y = 2 in x+y end end\n",new IntValue(62));
//		testCase("decl x = 1 in decl x = 2 in x*5 end + x end\n", new IntValue(11));
//		testCase("decl x = 1 y = 2 in decl x = 3 z = 4 in x + y + z end end\n",new IntValue(9));
//	}
}
